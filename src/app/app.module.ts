import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SobreComponent } from './sobre/sobre.component';
import { MenusComponent } from './navegacao/menus/menus.component';
import { FooterComponent } from './navegacao/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    SobreComponent,
    MenusComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
